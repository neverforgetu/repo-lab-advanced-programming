package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class HotClams implements Clams {
    public String toString() {
        return "Hot Clams from Keju Kraft Bay";
    }
}