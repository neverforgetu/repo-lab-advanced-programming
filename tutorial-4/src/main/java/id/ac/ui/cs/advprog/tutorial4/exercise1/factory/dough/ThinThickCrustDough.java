package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThinThickCrustDough implements Dough {
    public String toString() {
        return "Thin Thick Crust Dough Add";
    }
}