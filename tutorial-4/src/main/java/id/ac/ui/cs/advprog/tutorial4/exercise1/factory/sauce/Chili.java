package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class Chili implements Sauce {
    public String toString() {
        return "Chili Sauce";
    }
}