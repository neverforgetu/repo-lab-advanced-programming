package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinThickCrustDoughTest {
    private Dough dough;

    @Before
    public void setUp() {
        dough = new ThinThickCrustDough();
    }

    @Test
    public void testThinThickCrustDoughToStringMethod() {
        assertEquals(dough.toString(), "Thin Thick Crust Dough Add");
    }
}